use bytes::{Buf, BufMut};
use std::{
    io::{self, Read, Write},
    mem::size_of,
    net::TcpStream,
};

pub fn envoi(conn: &mut TcpStream, data: &[u8], sz: usize) -> Result<(), io::Error> {
    let mut pos = 0;
    let mut n = 1usize;
    while pos < sz && n > 0 {
        n = conn.write(&data[pos..sz])?;
        pos += n;
    }
    Ok(())
}

pub fn puts(conn: &mut TcpStream, data: &[u8]) -> Result<(), io::Error> {
    let i32sz = size_of::<i32>();
    let mut dlen: Vec<u8> = Vec::with_capacity(i32sz);
    dlen.put_i32_le(data.len() as i32);
    envoi(conn, &dlen, i32sz)?;
    envoi(conn, data, data.len())?;
    Ok(())
}

pub fn recoit(conn: &mut TcpStream, data: &mut [u8], sz: usize) -> Result<usize, io::Error> {
    let mut pos = 0;
    let mut n = 1usize;
    while pos < sz && n > 0 {
        n = conn.read(&mut data[pos..sz])?;
        pos += n;
    }
    Ok(pos)
}

pub fn agets(conn: &mut TcpStream) -> Result<Vec<u8>, io::Error> {
    let i32sz = size_of::<i32>();
    let mut dlen = vec![0u8; i32sz];
    recoit(conn, &mut dlen, i32sz)?;
    let length = dlen.as_slice().get_i32_le();
    if length <= 0 {
        return Ok(vec![0u8; 0]);
    }
    let mut data = vec![0u8; length as usize];
    recoit(conn, &mut data, length as usize)?;
    Ok(data)
}

pub fn gets(conn: &mut TcpStream, data: &mut [u8]) -> Result<usize, io::Error> {
    let i32sz = size_of::<i32>();
    let mut dlen = vec![0u8; i32sz];
    recoit(conn, &mut dlen, i32sz)?;
    let length = dlen.as_slice().get_i32_le();
    if length <= 0 {
        return Ok(0);
    }
    let mut length = length as usize;
    if length > data.len() {
        length = data.len();
    }
    recoit(conn, data, length)?;
    Ok(length)
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::{
        net::{TcpListener, TcpStream},
        sync::mpsc,
        thread,
    };
    #[test]
    fn it_works() {
        let addr = "127.0.0.1:7658";
        let ln = TcpListener::bind(addr).unwrap();
        let (tx, rx) = mpsc::channel();

        let _t = thread::spawn(move || {
            let mut stream = TcpStream::connect(addr).unwrap();

            puts(&mut stream, b"toto et tata font du ski").unwrap();

            let mut buf = [0u8; 17];
            let n = gets(&mut stream, &mut buf).unwrap();
            let s = String::from_utf8_lossy(&buf);
            tx.send(format!("[{}]#{}", s, n)).unwrap();
        });

        let mut stream = ln.accept().unwrap().0;

        let mut buf = [0u8; 24];
        let n = gets(&mut stream, &mut buf).unwrap();
        assert_eq!(n, 24);
        assert_eq!(&buf, b"toto et tata font du ski");

        puts(&mut stream, b"titi fait du velo").unwrap();
        let recv = rx.recv().unwrap();
        assert_eq!(recv, "[titi fait du velo]#17");
    }
}
